import java.util.*;
public class Java4 {
    public static void main(String[] args)  {
        Scanner in = new Scanner(System.in);
        int x = in.nextInt();
        boolean val = palindrome(x);
        System.out.println(val);
    }
    public static boolean palindrome(int x)
    {
        if(x<0) return false;
        int cpy = x;
        int rev = 0;
        while(cpy>0)
        {
            rev = rev*10 + cpy%10;
            cpy = cpy/10;
        }
        if(rev == x) return true;
        else return false;
    }
}