import java.util.*;
public class Java8 {
    public static void main(String[] args)  {
        Scanner in = new Scanner(System.in);
        int x = in.nextInt();
        int val = reverse(x);
        System.out.println(val);
    }
    public static int reverse(int x)
    {
        int cpy = x;
        int rev = 0;
        while(cpy>0)
        {
            rev = rev*10 + cpy%10;
            cpy = cpy/10;
        }
        return rev;
    }
}