import java.util.*;
public class Java7 {
    public static void main(String[] args)  {
        Scanner in = new Scanner(System.in);
        int x = in.nextInt();
        boolean val = ishappynumber(x);
        System.out.println(val);
    }
    static int numSquareSum(int n)
    {
        int squareSum = 0;
        while (n!= 0)
        {
            squareSum += (n % 10) * (n % 10);
            n /= 10;
        }
        return squareSum;
    }
    static boolean ishappynumber(int n)
    {
        int slow, fast;
        slow = fast = n;
        do
        {
            slow = numSquareSum(slow);
            fast = numSquareSum(numSquareSum(fast));
        }while (slow != fast);
        return (slow == 1);
    }
}
    